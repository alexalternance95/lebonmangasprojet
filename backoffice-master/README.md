# LebonMangas BACK-OFFICE

Ce projet utilise [Angular CLI](https://github.com/angular/angular-cli) version 7.1.0.

### Prérequis
W10 PRO / Linux
VisualStudio 2017
node js Version > 10.14.1
Docker version > 18.09.2
Angular v 7.1.0
Python

## Attention, si vous travailler sur un environnement Linux :
Lancer les commandes Docker en sudo.

## Environnmenet de Development 


### Important

```


```
Seb Service 
Secure Web Service
```

Naviguer dans le dossier backoffice.

Lancez `npm install @angular-devkit/build-angular` pour mettre à jour le parckage.

Lancez `ng serve` pour un serveur de développement. Naviguez vers `https://localhost:4200/`. 
Les pages ce rechargeront automatiquement lors de modificactions.

## Test de déploiement / Déploiement serveurs
Naviguez dans le dossier `backoffice`.



puis en fonction de l'environnement lancez :

dev : 
```
docker-compose build
docker-compose up -d
```

preprod : 
```
docker-compose -f docker-compose.preprod.yml build
docker-compose -f docker-compose.preprod.yml up -d
```
prod : 
```
docker-compose -f docker-compose.prod.yml build
docker-compose -f docker-compose.prod.yml up -d
```




## Commandes utiles

docker-compose up : build si besion puis lance le container
docker-compose build : build le container. options : --no-cache pour forcer le build.
docker-compose down : détruit les containers et leurs informations.
docker-compose stop : arrètes les containers.

commandes git :

git pull : récupère la dernière version de la branche.
git checkout nom_de_la_branche : change de branche et récupère la dernière version pull.
git fetch : récupère toutes les branches.
git add nom_du_fichier : ajoute le fichier dans le suivis git.
git comit -am "commentaire du comit" : enregistre les modifications en local pour pouvoir les push plus tard.
git push : envoie les modifications et met à jour la branche. si un conflit apparait, il faudra pull pour avoir la denière version.