import { Component, OnInit, Inject } from '@angular/core';
import {
  PageEvent, MatSort, MatTableDataSource, Sort, MatDatepickerInputEvent,
  DateAdapter, MAT_DATE_FORMATS, MatDialogRef, MAT_DIALOG_DATA, MatDialog, throwMatDialogContentAlreadyAttachedError
} from '@angular/material';
import { MenuComponent } from '../menu/menu.component';
export interface DialogData {

  result: boolean;
}

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})


export class MessageComponent implements OnInit {
  public gS:boolean = false;
  public mIF:boolean = false;
  public commandArray:any = [[/^\/bmsg\s+.+/i]];

  constructor(public dialogRef: MatDialogRef<MenuComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: DialogData,) { }

  ngOnInit(){
    this.onLoad();
  }
  
  clickHandler(){
    if(!this.gS){
        this.gS = true;      
        document.body.className += ' gSelect';
      }
      else{
        this.gS = false;      
        
      }
  }
  
  onModalCancel(): void {
    this.data.result = false;
    this.dialogRef.close(false);
  }

  inputFocusEventListener(){
    this.mIF=true;
  }

  inputBlurEventListener(){
    this.mIF=false;
  }

  keyPressEventHandler(e:any,msgInput:any){
    if(e.keyCode == 13 && this.mIF && msgInput.value){
        if(msgInput.value[0] === '/'){
          for(var i = 0; i < this.commandArray.length; i++){
            var match = this.commandArray[i][0].test(msgInput.value);
            if(match){
              //later
            }
          }
          if(match){
            document.getElementById('panel3').innerHTML += '<br/><div class="board"><div class="bName">Reego</div><div class="bText">' + msgInput.value.slice(5) + '</div></div>';
          }
          msgInput.value = '';
        }
        else{
        var msgArea = document.getElementById('msgArea');
        msgArea.innerHTML += '<div class="msg userMSG"><div></div><div>' + msgInput.value + '</div></div>';
        msgInput.value = '';
        }
      }
  }

  onLoad(){
    document.getElementById('sideBar').getElementsByTagName('div')[0].addEventListener('click',this.clickHandler);

    var msgInput = document.getElementById('msgInputWrap').getElementsByTagName('input')[0];
    msgInput.addEventListener('focus',this.inputFocusEventListener);
    msgInput.addEventListener('blur',this.inputBlurEventListener);
    window.addEventListener('keypress',(event)=>this.keyPressEventHandler(event,msgInput));

  }

}
