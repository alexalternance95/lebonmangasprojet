import { Component, OnInit } from '@angular/core';
import {
  PageEvent, MatSort, MatTableDataSource, Sort, MatDatepickerInputEvent,
  DateAdapter, MAT_DATE_FORMATS, MatDialogRef, MAT_DIALOG_DATA, MatDialog
} from '@angular/material';

@Component({
  selector: 'app-filtre',
  templateUrl: './filtre.component.html',
  styleUrls: ['./filtre.component.scss']
})
export class FiltreComponent implements OnInit {
  radioTitle: string;
  radioItems: Array<string>;
  model   = {option: 'option3'}

  constructor() { 
    this.radioTitle = 'Radio Button in Angular';
    this.radioItems = ['Offres', 'Demandes'];
  }

  ngOnInit() {
  }

}
