import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjoutPhotoAnnonceComponent } from './ajout-photo-annonce.component';

describe('AjoutPhotoAnnonceComponent', () => {
  let component: AjoutPhotoAnnonceComponent;
  let fixture: ComponentFixture<AjoutPhotoAnnonceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjoutPhotoAnnonceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjoutPhotoAnnonceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
