import { Component, OnInit, Inject } from '@angular/core';
import {
  PageEvent, MatSort, MatTableDataSource, Sort, MatDatepickerInputEvent,
  DateAdapter, MAT_DATE_FORMATS, MatDialogRef, MAT_DIALOG_DATA, MatDialog, throwMatDialogContentAlreadyAttachedError
} from '@angular/material';
import { MenuComponent } from '../menu/menu.component';
import { LocalisationAnnonceComponent } from '../localisation-annonce/localisation-annonce.component';
export interface DialogData {

  result: boolean;
}
@Component({
  selector: 'app-ajout-photo-annonce',
  templateUrl: './ajout-photo-annonce.component.html',
  styleUrls: ['./ajout-photo-annonce.component.scss']
})
export class AjoutPhotoAnnonceComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<MenuComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: DialogData, public dialog: MatDialog) { }

  ngOnInit() {
  }
  onModalCancel(): void {
    this.data.result = false;
    this.dialogRef.close(false);
  }
  onOpenSuivantModal(): void{
    this.data.result = false;
    this.dialogRef.close(false);
    const dialogRef = this.dialog.open(LocalisationAnnonceComponent, {
      disableClose: true,
      data: {
         
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });

  }
}
