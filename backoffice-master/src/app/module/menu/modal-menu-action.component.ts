import { Component, OnInit, Inject, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import {
  PageEvent, MatSort, MatTableDataSource, Sort, MatDatepickerInputEvent,
  DateAdapter, MAT_DATE_FORMATS, MatDialogRef, MAT_DIALOG_DATA, MatDialog
} from '@angular/material'
import { Router } from '@angular/router';
import { MenuComponent } from './menu.component';

export interface DialogData {

  result: boolean;
}

@Component({
  selector: 'app-modal-menu-action',
  templateUrl: './modal-menu-action.component.html'
})
export class ModalMenuActionComponent implements OnInit{
  result: any;


  constructor(public dialog: MatDialog, 
    public dialogRef: MatDialogRef<MenuComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: DialogData ) {
  }

  ngOnInit() {  
    this.dialogRef.updateSize("90%","87%"); 
  }




  onModalConfirm(): void {
    this.data.result = true;
    this.dialogRef.close(true);
  }



}
