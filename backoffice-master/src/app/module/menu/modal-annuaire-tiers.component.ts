import { Component, OnInit, Inject } from '@angular/core';
import {
  PageEvent, MatSort, MatTableDataSource, Sort, MatDatepickerInputEvent,
  DateAdapter, MAT_DATE_FORMATS, MatDialogRef, MAT_DIALOG_DATA, MatDialog
} from '@angular/material';
import { MenuComponent } from './menu.component';

import { Router } from '@angular/router';




export interface DialogData {

  result: boolean;
}

@Component({
  selector: 'app-modal-annuaire-tiers',
  templateUrl: './modal-annuaire-tiers.component.html'
})
export class ModalAnnuaireTiersComponent implements OnInit {


 



  constructor(public dialog: MatDialog,
    private router: Router,
    public dialogRef: MatDialogRef<MenuComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
   
    ) {
    }



    

  
  ngOnInit() {
    this.dialogRef.updateSize("90%","87%");
  }


  onModalCancel(): void {
    this.data.result = false;
    this.dialogRef.close(false);
  }

  onModalConfirm(): void {
    this.data.result = true;
    this.dialogRef.close(true);
  }

}
