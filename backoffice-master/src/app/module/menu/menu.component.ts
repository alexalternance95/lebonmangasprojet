import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import {
  PageEvent, MatSort, MatTableDataSource, Sort, MatDatepickerInputEvent,
  DateAdapter, MAT_DATE_FORMATS, MatDialogRef, MAT_DIALOG_DATA, MatDialog
} from '@angular/material';
import { MessageComponent } from '../message/message.component';
import { DepoAnnonceComponent } from '../depo-annonce/depo-annonce.component';
import { FavorisComponent } from '../favoris/favoris.component';

export interface DialogData {
  animal: string;
  name: boolean;
}

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})



export class MenuComponent implements OnInit {

  constructor(private router: Router,
    public dialog: MatDialog) {
  }

  ngOnInit() {
  }



  openMessage(): void {
    const dialogRef = this.dialog.open(MessageComponent, {
      disableClose: true,
      width: '2000px',
      height: '1000px',
      data: {
         
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openDepoAnnonce(): void {
    const dialogRef = this.dialog.open(DepoAnnonceComponent, {disableClose: true,
     
      data: {
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
  openMenuFavoris(): void {
    const dialogRef = this.dialog.open(FavorisComponent, {disableClose: true,
      data: {
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }



}
