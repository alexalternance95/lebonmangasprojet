import { Component, OnInit, Inject } from '@angular/core';
import {
  PageEvent, MatSort, MatTableDataSource, Sort, MatDatepickerInputEvent,
  DateAdapter, MAT_DATE_FORMATS, MatDialogRef, MAT_DIALOG_DATA, MatDialog, throwMatDialogContentAlreadyAttachedError
} from '@angular/material';
import { MenuComponent } from './menu.component';
import { dateToLocalArray } from '@fullcalendar/core/datelib/marker';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


export interface DialogData {

  result: boolean;
}

@Component({
  selector: 'app-modal-prise-appel',
  templateUrl: './modal-prise-appel.component.html'
})
export class ModalPriseAppelComponent implements OnInit {


  constructor(
    
    private router: Router,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<MenuComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }





  ngOnInit() {
   this.dialogRef.updateSize("90%","85%");
  }

  onModalCancel(): void {
    this.data.result = false;
    this.dialogRef.close(false);
  }

  onModalConfirm(): void {
    this.data.result = true;
    this.dialogRef.close(true);
  }

}
