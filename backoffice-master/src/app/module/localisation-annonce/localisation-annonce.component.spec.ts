import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalisationAnnonceComponent } from './localisation-annonce.component';

describe('LocalisationAnnonceComponent', () => {
  let component: LocalisationAnnonceComponent;
  let fixture: ComponentFixture<LocalisationAnnonceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalisationAnnonceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalisationAnnonceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
