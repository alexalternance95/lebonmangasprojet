import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepoAnnonceComponent } from './depo-annonce.component';

describe('DepoAnnonceComponent', () => {
  let component: DepoAnnonceComponent;
  let fixture: ComponentFixture<DepoAnnonceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepoAnnonceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepoAnnonceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
