import { Component, OnInit, Inject } from '@angular/core';
import {
  PageEvent, MatSort, MatTableDataSource, Sort, MatDatepickerInputEvent,
  DateAdapter, MAT_DATE_FORMATS, MatDialogRef, MAT_DIALOG_DATA, MatDialog, throwMatDialogContentAlreadyAttachedError
} from '@angular/material';
import { MenuComponent } from '../menu/menu.component';
import {FormControl} from '@angular/forms';

import { AjoutPhotoAnnonceComponent } from '../ajout-photo-annonce/ajout-photo-annonce.component';
export interface DialogData {

  result: boolean;
}

@Component({
  selector: 'app-depo-annonce',
  templateUrl: './depo-annonce.component.html',
  styleUrls: ['./depo-annonce.component.scss']
})
export class DepoAnnonceComponent implements OnInit {
  radioTitle: string;
  radioItems: Array<string>;
  model   = {option: 'option3'}
  radioTitles: string;
  radioItemss: Array<string>;
  models   = {option: 'option3'}

  constructor(public dialogRef: MatDialogRef<MenuComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: DialogData,public dialog: MatDialog) {this.radioTitle = 'Radio Button in Angular';
    this.radioItems = ['Offres', 'Demandes'];
  this.radioTitles ="test";
this.radioItemss = ['Mondial Relay', 'Colissimo', 'Autres moyen de livraison']
 }

  ngOnInit() {
  }
  toppings = new FormControl();
  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
     
  onModalCancel(): void {
    this.data.result = false;
    this.dialogRef.close(false);
  }
  value = '';
  valuee ='';

  onOpenSuivantModal(): void{
    this.data.result = false;
    this.dialogRef.close(false);
    const dialogRef = this.dialog.open(AjoutPhotoAnnonceComponent, {
      disableClose: true,
      data: {
         
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });

  }
}
