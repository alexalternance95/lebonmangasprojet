import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FullCalendarComponent } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction';
import listPlugin from '@fullcalendar/list'
import bootstrapPlugin from '@fullcalendar/bootstrap';
import timeGrigPlugin from '@fullcalendar/timegrid';
import { EventInput } from '@fullcalendar/core';
import { MatDialog } from '@angular/material';
import { Router, NavigationEnd } from '@angular/router';
import { tap } from 'rxjs/internal/operators';
import { forkJoin } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { createTitle } from '@angular/platform-browser/src/browser/title';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.scss']
})

export class PrincipalComponent implements OnInit {
  options: any;


  constructor(public dialog: MatDialog,
  private router: Router,
) { 

  }
  showVar: boolean = false;

  toggleChild(){
      this.showVar = !this.showVar;
  }






  ngOnInit() {

  }



}
