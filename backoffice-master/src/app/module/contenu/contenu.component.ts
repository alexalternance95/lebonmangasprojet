import { Component, OnInit, Input } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ModalContenuComponent } from './modal-contenu/modal-contenu.component';

@Component({
  selector: 'app-contenu',
  templateUrl: './contenu.component.html',
  styleUrls: ['./contenu.component.scss']
})
export class ContenuComponent implements OnInit {
  @Input() showMePartially: boolean;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(ModalContenuComponent, {
      width: '1000px',
      height: '1000px',
      data: {},
    
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
     
    });
  }
}
