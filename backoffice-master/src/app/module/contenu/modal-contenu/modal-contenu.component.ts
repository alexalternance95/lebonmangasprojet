import { Component, OnInit, Inject } from '@angular/core';
import {
  PageEvent, MatSort, MatTableDataSource, Sort, MatDatepickerInputEvent,
  DateAdapter, MAT_DATE_FORMATS, MatDialogRef, MAT_DIALOG_DATA, MatDialog, throwMatDialogContentAlreadyAttachedError
} from '@angular/material';
import { MenuComponent } from '../../menu/menu.component';
export interface DialogData {

  result: boolean;
}
@Component({
  selector: 'app-modal-contenu',
  templateUrl: './modal-contenu.component.html',
  styleUrls: ['./modal-contenu.component.scss']
})
export class ModalContenuComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<MenuComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit() {
  }
  onModalCancel(): void {
    this.data.result = false;
    this.dialogRef.close(false);
  }

}
