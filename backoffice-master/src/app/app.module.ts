import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ToastrModule } from 'ngx-toastr';
import { MenuComponent } from './module/menu/menu.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import { ModalPriseAppelComponent } from './module/menu/modal-prise-appel.component';
import {
  DateAdapter,
  MAT_DATE_LOCALE,
  MatDatepickerModule, MatExpansionModule, MatInputModule, MatNativeDateModule, MatPaginatorIntl, MatPaginatorModule, MatSortModule,
  MatTableModule, MatDialogModule, MatCheckboxModule, MatDividerModule, MatButtonModule, MatTabsModule, MatToolbarModule, MatCardModule,
  MatSidenavModule, MatSlideToggleModule, MatListModule, MatFormFieldModule, MatSelectModule, MatAutocompleteModule, MatIconModule, MatRadioModule, MatStepperModule, MatMenuModule, MAT_DATE_FORMATS, MatTooltip, MatTooltipModule

} from '@angular/material';



import { AuthGuard } from './auth.guard';
import { LayoutModule } from '@angular/cdk/layout';
import { MatGridListModule } from '@angular/material/grid-list';
import { ModalAnnuaireTiersComponent } from './module/menu/modal-annuaire-tiers.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { ParametreComponent } from './module/parametre/parametre.component';
import { PrincipalComponent } from './module/principal/principal.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { ContenuComponent } from './module/contenu/contenu.component';
import { FiltreComponent } from './module/filtre/filtre.component';
import { ModalContenuComponent } from './module/contenu/modal-contenu/modal-contenu.component';
import { LoginComponent } from './module/login/login.component';
import { MessageComponent } from './module/message/message.component';
import { DepoAnnonceComponent } from './module/depo-annonce/depo-annonce.component';
import { AjoutPhotoAnnonceComponent } from './module/ajout-photo-annonce/ajout-photo-annonce.component';
import { LocalisationAnnonceComponent } from './module/localisation-annonce/localisation-annonce.component';
import { FavorisComponent } from './module/favoris/favoris.component';



// the second parameter 'fr-FR' is optional
registerLocaleData(localeFr, 'fr-FR');


export function provideConfig() {
  return;
}
@NgModule({
  entryComponents: [
      ModalContenuComponent,
    LoginComponent,
    MessageComponent,
    DepoAnnonceComponent,
    AjoutPhotoAnnonceComponent,
    LocalisationAnnonceComponent,
    FavorisComponent
  ],
  declarations: [
 
    AppComponent,
    MenuComponent,
    ModalAnnuaireTiersComponent,
    ModalPriseAppelComponent,
    ParametreComponent,
    PrincipalComponent,
    ContenuComponent,
    FiltreComponent,
    ModalContenuComponent,
    LoginComponent,
    MessageComponent,
    DepoAnnonceComponent,
    AjoutPhotoAnnonceComponent,
    LocalisationAnnonceComponent,
    FavorisComponent,

  ],

  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    FullCalendarModule,
    HttpClientModule,
    LayoutModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatTooltipModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatSelectModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    OverlayModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
  ],

  providers: [
        AuthGuard,
      ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
