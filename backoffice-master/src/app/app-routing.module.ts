import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrincipalComponent } from './module/principal/principal.component';

import { ParametreComponent } from './module/parametre/parametre.component';

import { AuthGuard } from './auth.guard';


const routes: Routes = [
  { path: '', component: PrincipalComponent,
    canActivate: [AuthGuard],
    runGuardsAndResolvers: 'always'},
    { path: 'parametre', component: ParametreComponent, canActivate: [AuthGuard]}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'},
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
