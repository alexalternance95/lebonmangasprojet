import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OverlayContainer } from '@angular/cdk/overlay';
import {MediaMatcher} from '@angular/cdk/layout';
import {ChangeDetectorRef} from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { LoginComponent } from './module/login/login.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  showModeurPanel: boolean;
  showAdminPanel: boolean;

  private _mobileQueryListener: () => void;
  mobileQuery: MediaQueryList;
  
  constructor(
    private overlay: OverlayContainer,
    public router: Router,
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher, public dialog: MatDialog) {
      this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }





  ngOnInit() {
  }


  reloadChild() {
    this.router.navigate(['/']);
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(LoginComponent, {
      width: '1000px',
      height: '1000px',
      data: {},
    
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
     
    });
  }

  
  

}

